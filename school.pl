teacher(mr_Knight).
teacher(ms_Gross).
teacher(mr_McEvoy).
teacher(ms_Appleton).
teacher(ms_Parnell).

subject(maths).
subject(science).
subject(history).
subject(english).
subject(pe).

county(suffolk).
county(cornwall).
county(norfolk).
county(yorkshire).
county(cumbria).
county(hertfordshire).

activity(nudistColony).
activity(body-boarding).
activity(swimming).
activity(sightseeing).
activity(camping).

solve :- 
	subject(KnightSubject), subject(GrossSubject), subject(McEvoySubject),
	subject(AppletonSubject), subject(ParnellSubject),
	all_different([KnightSubject, GrossSubject, McEvoySubject, AppletonSubject, 		
	ParnellSubject]),
	
	county(KnightCounty), county(GrossCounty), county(McEvoyCounty),
	county(AppletonCounty),county(ParnellCounty),
	all_different([KnightCounty, GrossCounty, McEvoyCounty, AppletonCounty, 	
	ParnellCounty]),

	activity(KnightActivity), activity(GrossActivity), activity(McEvoyActivity),
	activity(AppletonActivity), activity(ParnellActivity),
	all_different([KnightActivity, GrossActivity, McEvoyActivity, AppletonActivity, 
	ParnellActivity]),

	Quads = [ [mr_Knight, KnightSubject, KnightCounty, KnightActivity],
	          [ms_Gross, GrossSubject, GrossCounty, GrossActivity],
		  [mr_McEvoy, McEvoySubject, McEvoyCounty, McEvoyActivity],
		  [ms_Appleton, AppletonSubject, AppletonCounty, AppletonActivity],
		  [ms_Parnell, ParnellSubject, ParnellCounty, ParnellActivity] ],
		
	%Ms. Gross teaches maths or science.
	(member([ms_Gross, maths, _, _], Quads);
	member([ms_Gross, science, _, _], Quads)),

	%Ms. Gross is going to suffolk or Cornwall.
	(member([ms_Gross, _, cornwall, _], Quads);
	member([ms_Gross, _, suffolk, nudistColony], Quads)),
	
	%MS. Gross is going to suffolk if she is going to a nudist colony.
	\+ member([ms_Gross, _, cornwall, nudistColony], Quads),

	%The science teacher is going body0boarding and either cornwall or norfolk.
	(member([_, science, cornwall, body-boarding], Quads);
	member([_, science, norfolk, body-boarding], Quads)),
	
	%Mr. McEvoy is the history teacher and is going to either Yorkshire or Cumbria.
	(member([mr_McEvoy, history, yorkshire, _], Quads);
	member([mr_McEvoy, history, cumbria, _], Quads)),

	%If the woman who is going to Hertfordshire is the English teacher, then she is Ms. Appleton; otherwise, she is Ms. Parnell.
	(member([ms_Appleton, english, hertfordshire, _], Quads);
	member([ms_Parnell, _, hertfordshire, _], Quads)),
	
	%Ms. Parnell is going Swimming.
	member([ms_Parnell, _, _, swimming], Quads),
	\+ member([ms_Parnell, english, Hertfordshire, _], Quads),
	
	% The person going to yorkshire is not the Pe teacher and is not going sightseeing. 
	\+ member([_, pe, yorkshire, _], Quads),
	\+ member([_, _, yorkshire, sightseeing], Quads),
	member([_, _, yorkshire, _], Quads),
	
	% Ms. Gross is not the WOMAN going camping (and Ms. Parnell is going swimming).
	member([ms_Appleton, _, _, camping], Quads),

	%One woman is going to a nudist colony.
	\+ member([mr_McEvoy, _, _, nudistColony], Quads),
	\+ member([mr_Knight, _, _, nudistColony], Quads),

	tell(mr_Knight, KnightSubject, KnightCounty, KnightActivity),
	tell(ms_Gross, GrossSubject, GrossCounty, GrossActivity),
	tell(mr_McEvoy, McEvoySubject, McEvoyCounty, McEvoyActivity),
	tell(ms_Appleton, AppletonSubject, AppletonCounty, AppletonActivity),
	tell(ms_Parnell, ParnellSubject, ParnellCounty, ParnellActivity).


% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(W, X, Y, Z) :-
	write(W), write(' teaches '), write(X), write(' and will be going to '), 
	write(Y), write(' to participate in '), write(Z), write('.'), nl.